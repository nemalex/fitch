CC = cc
CFLAGS = -pedantic -W -Wall
PREFIX = /usr/local

fitch: fitch.c
	$(CC) $(CFLAGS) $? -o $@

install: fitch
	mkdir -p ${PREFIX}/bin
	cp -f fitch ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/fitch

uninstall:
	rm -f ${PREFIX}/bin/fitch

clean:
	rm -f fitch

debug: fitch.c
	$(CC) $(CFLAGS) -g $? -o fitch
	valgrind --track-origins=yes --leak-check=full --show-leak-kinds=all -s ./fitch

.PHONY: clean
