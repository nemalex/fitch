#include <linux/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/utsname.h>
#include <sys/sysinfo.h>

#define RED			"\33[1;91m"
#define GREEN		"\33[1;92m"
#define YELLOW		"\33[1;93m"
#define BLUE		"\33[1;94m"
#define MAGENTA		"\33[1;95m"
#define CYAN		"\33[1;96m"
#define WHITE		"\33[1;39m"
#define RESET		"\033[m"

void
trim_to_last(char* str, char ch, char **ret){
	int i = 0;
	while (str[i] != '\0'){
		if (str[i] == ch)
			*ret = &str[i]+1;
		i++;
	}
}

void
trim_trailing(char* str){
	int index, i;
	index = -1;
	i = 0;
	while (str[i] != '\0'){
		if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
			index= i;
		i++;
	}
	str[index + 1] = '\0';
}

void
format_uptime(long uptime, char **uptime_formatted){
	long n = uptime;
	static char buf[16];
	int day = n / (60 * 60 * 24);
	n -= day * (60 * 60 * 24);
	int hour = n / (60 * 60);
	n -= hour * (60 * 60);
	int min = n / 60;
	n -= min * 60;
	int sec = n;
	if (uptime < 60){
		sprintf(buf, "%ds",sec);
		*uptime_formatted = buf;
		return;
	}
	if (day > 0)
		sprintf(buf + strlen(buf), "%dd ", day);
	if (hour > 0)
		sprintf(buf + strlen(buf), "%dh ", hour);
	if (min > 0)
		sprintf(buf + strlen(buf), "%dm", min);
	trim_trailing(buf);
	*uptime_formatted = buf;
}

void
get_title(char **title){
	static char buf[512];
	char *username = getlogin();
	char hostname[HOST_NAME_MAX];
	if (gethostname(hostname, HOST_NAME_MAX) == -1){
		fprintf(stderr, "ERROR: Failed to get hostname");
		exit(1);
	}
	sprintf(buf,"%s@%s", username, hostname);
	*title = buf;
}

// TODO android detection
//  -d /system/app/ && -d /system/priv-app
// "Android $(getprop ro.build.version.release)"
void
get_distro(char **distro){
	char *os_name = "";
	char *os_major = "";
	FILE *f = fopen("/etc/os-release", "r");
	if (f == 0){
		fprintf(stderr, "ERROR: Failed to read distro type");
		exit(1);
	} else { 
		char *p, buf[512];
		while (fgets(buf, sizeof(buf), f)) {
			char *value, *q;
			if (buf[0] == '#')
				continue;
			p = strchr(buf, '=');
			if (!p)
				continue;
			*p++ = 0;
			value = p;
			q = p;
			while (*p) {
				if (*p == '\\') {
					++p;
					if (!*p)
						break;
					*q++ = *p++;
				} else if (*p == '\'' || *p == '"' ||
					   *p == '\n') {
					++p;
				} else {
					*q++ = *p++;
				}
			}
			*q = 0;
			if (!strcmp(buf, "NAME")) {
				p = strdup(value);
				if (!p)
					break;
				os_name = p;
			} else if (!strcmp(buf, "VERSION_ID")) {
				p = strdup(value);
				if (!p)
					break;
				os_major = p;
			}
		}
	}
	fclose(f);
	*distro = strcat(os_name, os_major);
}

void
get_kernel(char **kernel){
	static struct utsname os_info;
	if (uname(&os_info) < 0){
		fprintf(stderr, "ERROR: Failed to get os type");
		exit(1);
	}
	*kernel = os_info.release;
}

// TODO raspberry pi model, maybe stole from neofetch
// TODO android model
// model="$(getprop ro.product.brand) $(getprop ro.product.model)"
void
get_hardware_model(char **model){
	FILE* model_fp;
	char model_filename[3][256] = {"/sys/devices/virtual/dmi/id/product_version",
	                               "/sys/devices/virtual/dmi/id/product_name",
	                               "/sys/devices/virtual/dmi/id/board_name"};
	static char tmp_model[3][256];
	int longest_model = 0, best_len = 0, currentlen = 0;
	for (int i = 0; i < 3; i++) {
		model_fp = fopen(model_filename[i], "r");
		if (model_fp) {
			fgets(tmp_model[i], 256, model_fp);
			tmp_model[i][strlen(tmp_model[i]) - 1] = '\0';
			fclose(model_fp);
		}
		currentlen = strlen(tmp_model[i]);
		if (currentlen > best_len) {
			best_len	  = currentlen;
			longest_model = i;
		}
	}
	*model = tmp_model[longest_model];
}

void
get_shell(char **shell){
	trim_to_last(getenv("SHELL"), '/', shell);
}

void
get_memory_usage(int **ram_used, int **ram_total){
	FILE* meminfo = fopen("/proc/meminfo", "r");
	char buffer[256];
	int memtotal = 0, shmem = 0, memfree = 0, buffers = 0, cached = 0, sreclaimable = 0;
	while (fgets(buffer, sizeof(buffer), meminfo)) {
		sscanf(buffer, "MemTotal:       %d", &memtotal);
		sscanf(buffer, "Shmem:             %d", &shmem);
		sscanf(buffer, "MemFree:        %d", &memfree);
		sscanf(buffer, "Buffers:          %d", &buffers);
		sscanf(buffer, "Cached:          %d", &cached);
		sscanf(buffer, "SReclaimable:     %d", &sreclaimable);
	}
	fclose(meminfo);
	static int used, total;
	used = ((memtotal + shmem) - (memfree + buffers + cached + sreclaimable)) / 1024;
	total = memtotal / 1024;
	*ram_used = &used;
	*ram_total = &total;
}

void
get_uptime(char **uptime){
	static struct sysinfo info;
	if (sysinfo(&info) < 0){
		fprintf(stderr, "ERROR: Failed to get system information");
		exit(1);
	}
	format_uptime(info.uptime, uptime);
}

void
get_package_count(unsigned int **pkg_count){
	FILE *f;
	static unsigned int c;
	char *pkgmans[] = {
		"pacman -Qq 2> /dev/null | wc -l",
		"dpkg-query -W 2> /dev/null | wc -l",
	};
	long unsigned int i = 0;
	while (i < sizeof(pkgmans) && !c){
		f = popen(pkgmans[i], "r");
		fscanf(f, "%u", &c);
		++i;
	}
	*pkg_count = &c;
}

// TODO add get_cpu function

// TODO correct memory leak

int
main(void) {
	char *title;
	char *distro;
	char *kernel;
	char *model;
	char *shell;
	int *ram_used;
	int *ram_total;
	char *uptime;
	unsigned int *pkg_count;

	get_title(&title);
	get_distro(&distro);
	get_kernel(&kernel);
	get_hardware_model(&model);
	get_shell(&shell);
	get_memory_usage(&ram_used, &ram_total);
	get_uptime(&uptime);
	get_package_count(&pkg_count);

	printf("\n");
	printf("%s%s%s\n", WHITE, title, RESET);
	printf("%sdistro:%s\t  %s\n", RED, RESET, distro);
	printf("%skernel:%s\t  %s\n", GREEN, RESET, kernel);
	printf("%smodel:%s\t  %s\n", YELLOW, RESET, model);
	printf("%sshell:%s\t  %s\n", BLUE, RESET, shell);
	printf("%smemory:%s\t  %d MiB / %d MiB\n", MAGENTA, RESET, *ram_used, *ram_total);
	printf("%suptime:%s\t  %s\n", CYAN, RESET, uptime);
	printf("%spkgs:%s\t  %u\n", WHITE, RESET, *pkg_count);
	printf("\n");

	return 0;
}
